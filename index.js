
let Promise = require('promise');
let fs = require('fs');
let request = require('request');
let FeedParser = require('feedparser');
let Iconv = require('iconv').Iconv;
let createTextVersion = require('textversionjs');
let decode = require('ent/decode');
let createCsvWriter = require('csv-write-stream');
let writer = createCsvWriter();

//

let outputPath = 'output.csv';
writer.pipe(fs.createWriteStream(outputPath));

let pattern = /(?:Incident Type:\s*)(.+)\s*(ILLINOIS INSTITUTE OF TECHNOLOGY\s*(?:\w|:|-|\/|\s+)*?(?=\s*(?:Date\/Time Reported:\s*)?(?:\d{1,2}\/\d{1,2}\/\d{4})))(?:(?:Date\/Time Reported:)?\s*(\d{1,2}\/\d{1,2}\/\d{4}))\s+(\d{2}:\d{2}\s+(?:AM|PM))\s*(?:Disposition:\s*(.+))?\s*(?:Notes:\s*(.+))?/gm;

function fetch(feed, callback) {
  console.log('fetching: ' + feed);

  // Define our streams
  var req = request(feed, {timeout: 10000, pool: false});
  req.setMaxListeners(50);
  // Some feeds do not respond without user-agent and accept headers.
  req.setHeader('user-agent', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.63 Safari/537.36');
  req.setHeader('accept', 'text/html,application/xhtml+xml');

  var feedparser = new FeedParser();

  // Define our handlers
  req.on('error', fail);
  req.on('response', function(res) {
    if (res.statusCode != 200) return this.emit('error', new Error('Bad status code: ' + res.statusCode));
    var charset = getParams(res.headers['content-type'] || '').charset;
    res = maybeTranslate(res, charset);
    // And boom goes the dynamite
    res.pipe(feedparser);
  });

  feedparser.on('error', fail);
  // feedparser.on('end', () => {});
  feedparser.on('readable', function() {
    var post;
    while (post = this.read()) {
      let title = post.title;
      let content = post.description;
      let text =
        decode(createTextVersion(content))
        .replace(/–/g, '-')
        .replace(/<sup>/g,'')
        .replace(/<\/sup>/g,'');

      let groups = [];
      let reports = [];
      while ((groups = pattern.exec(text)) !== null) {
        reports.push({
          'incident': groups[1],
          'location': groups[2].replace(/\n/g, ''),
          'date': groups[3],
          'time': groups[4],
          'disposition': groups[5],
          'notes': groups[6]
        });
      }

      let write = function(obj){
        return new Promise(function (resolve, reject){
          writer.write(obj, function () {
            resolve();
          });
        });
      }

      let onWrite = (x) => {
        return write(x).then(() => {
          console.log('write: ' + x.date + ' ' + x.time);
        })
      }

      let writes = reports.map(onWrite);
      writes.reduce((p, fn) => p.then(fn), Promise.resolve())
        .then(callback);

      //fs.appendFileSync(outputPath, JSON.stringify(results) + '\r\n', 'utf8');
      //console.log(title + '\r\n' + text);
    }
  });
}

function maybeTranslate (res, charset) {
  var iconv;
  // Use iconv if its not utf8 already.
  if (!iconv && charset && !/utf-*8/i.test(charset)) {
    try {
      iconv = new Iconv(charset, 'utf-8');
      console.log('Converting from charset %s to utf-8', charset);
      iconv.on('error', done);
      // If we're using iconv, stream will be the output of iconv
      // otherwise it will remain the output of request
      res = res.pipe(iconv);
    } catch(err) {
      res.emit('error', err);
    }
  }
  return res;
}

function getParams(str) {
  var params = str.split(';').reduce(function (params, param) {
    var parts = param.split('=').map(function (part) { return part.trim(); });
    if (parts.length === 2) {
      params[parts[0]] = parts[1];
    }
    return params;
  }, {});
  return params;
}

function fail(err) {
  if (err) {
    console.log(err, err.stack);
    return process.exit(1);
  }

  process.exit();
}

function delay(ms) {
  return () => {
    return new Promise(function(resolve, reject){
      setTimeout(() => {
        resolve();
      }, ms)
    });
  };
}

let urls = []
let maxPaged = 200; // TODO: too lazy to figure out when we run out of pages 
let baseUrl = "http://blogs.iit.edu/public_safety/feed/";
for (var paged = 1; paged <= maxPaged; paged++) {
  var url = baseUrl;
  if (paged > 1) {
    url += "?paged=" + paged.toString();
  }
  urls.push(url);
}

let pfetch = Promise.denodeify(fetch, 1);
function onFetch(url) {
  return () => pfetch(url).then(delay(1000));
}

let promises = urls.map(onFetch);
promises.reduce((p, fn) => p.then(fn), Promise.resolve());
