# A Web Scraper for IIT Public Safety Department's Crime Log

## CS100 at Illinois Tech
### by Josh Bowden and Taylor Berg


## Overview 
As freshmen at Illinois Tech, we tend to be curious and interested in exploring some of the quirky parts of the university. For better or worse, one of the interesting things was discovering the campus's public safety department's blog detailing the university's crime log. Notably, the crime log in text format goes back to March of 2014 while previous posts link to a generated PDF.

From a technical standpoint, this was an interesting challenge since the text format of the log was somewhat inconsistent and also needed to be scraped from the blog from page to page.

To do so, the scraper:

 * fetches pages from the blog's RSS feed to get the content from the posts
 * ...which is then converted from HTML to text only
 * ...which is then processed with a [279 character regex (!)][regex.txt] to parse out all the fields for each incident
 * ...which is then written to a CSV that can be opened with Excel.

 [regex.txt]: https://bitbucket.org/jaxrtech/iitpsd-crime-log-scraper/src/ed285c137debb541080d0d7c0900c694bbc9849c/regex.tx

## Data Analysis

### Hypothesis
Our hypothesis was that the most common form of incident reported by IITPSD was *theft* as to confirm the data reported by the [Chicago Tribune's "Crime in Chicagoland" Interactive](http://crime.chicagotribune.com/chicago/community/douglas#?address=60616) for the 60616 zipcode the Mies Campus resides in.

### Approach
Using our web scraper script, the output CSV file was ready to be opened with Excel. A few rows had to be manually adjusted to due human error. Additionally, since the `incident` column details the specific incident in a tree-like format, we used a formula to simply keep the "root" category for comparison purposes. This `category` column could then be used in a PivotTable to run a count to group by each incident's category.

### Conclusion
Our hypothesis was partially correct. In terms of *all* incidents reported by the IIPSD, the most common incident was under the category of `INJURED/SICK PERSON` with 350 instances mainly for medical transport. In terms of *criminal* incidents, `LARCENY/THEFT` is the most common incident with 168 incidents.

For quick reference, the top 10 categories from the analysis are shown below.

```
INJURED/SICK PERSON.............340
ALARM...........................203
LARCENY/THEFT...................168
UTILITY INCIDENT................108
DAMAGE TO PROPERTY...............85
ACCIDENT.........................82
ADMINISTRATIVE INFORMATION.......69
DISTURBANCE......................68
ROBBERY..........................63
TRESPASSING......................54
```